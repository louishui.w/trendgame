const ctx = document.getElementById('myChart').getContext('2d');
const data = {
        labels: [], // times per 4second
        datasets: [
                {
                        // fill: {
                        //         target: 'origin',
                        //         // above: 'rgb()',   // Area will be red above the origin
                        //         // below: 'rgb(0, 0, 255)'    // And blue below the origin
                        // },
                        label: '$$',
                        data: [],//[12, 5, 6], //price
                        backgroundColor: [

                                "rgba(39, 40, 44, 1.0)",
                                'rgba(39, 40, 44, 1.0)',
                                'rgba(39, 40, 44, 1.0)',

                        ],
                        borderColor: [
                                'rgba()',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                        ],

                }
        ]
};
const config = {
        type: 'line',
        data: data,
        options: {
                animation: false,
        }

};
const newChart = new Chart(ctx, config);


///////////////////function /////////////
///////////////////function /////////////
///////////////////function /////////////
let base = 100;
// let x = Object.entries(data)[1][1][0];
// console.log(typeof (x))
let targetPriceArr = newChart.data.datasets[0].data;
// newchart.config.data.datasets[0].data
let targetDateArr = newChart.data.labels;
let balance = 100;
document.getElementById("balance").textContent = balance

let bet = 0;
let buy = false;
let sell = false;
let profitOrLossForRun = 0;

///buyBtnFunction
//get value from input text
//cal bala & bet

function handleBuyBtn() {

        buy = true;
        console.log(buy);

        let inputEl = document.getElementById('howMuch');
        let howMuchValue = parseInt(inputEl.value); //


        console.log(howMuchValue);

        if (inputEl.value === "" || isNaN(howMuchValue)) {
                alert("no empty input , not a number ")
                return;
        }
        else if (inputEl.value > balance) {
                alert("not enough money")
                return
        }
        else {
                console.log(balance, howMuchValue);
                balance -= howMuchValue;
                bet += howMuchValue;
                console.log(`bal:${balance}`);
                console.log(`bet:${bet}`);
                document.getElementById('betValue').textContent = bet;
                document.getElementById('balance').textContent = balance;
                // console.log(targetPriceArr[targetPriceArr.length-1]);

                // console.log(targetPriceArr[targetPriceArr.length-2]);


        }





}

function handleSellBtn() {
        sell = true;
        console.log(sell);

        let inputEl = document.getElementById('howMuch');
        let howMuchValue = parseInt(inputEl.value);


        if (inputEl.value === "" || isNaN(howMuchValue || inputEl.value > currentBalance)) {
                return;
        } else {
                balance -= howMuchValue;
                bet += howMuchValue;
                console.log(`bal:${balance}`);
                console.log(`bet:${bet}`);
                document.getElementById('betValue').textContent = bet;
                document.getElementById('balance').textContent = balance;
                // console.log(targetPriceArr[targetPriceArr.length-1]);

                // console.log(targetPriceArr[targetPriceArr.length-2]);

        }


}
//calu Profit & Loss

function caluProfitLoss() {
        profitOrLossForRun = 0;
        console.log(profitOrLossForRun)
        let oldPriceFromPriceArr = targetPriceArr[targetPriceArr.length - 1];
        let newPricecFromPriceArr = targetPriceArr[targetPriceArr.length - 2];

        // console.log(lots);
        // console.log(bet);
        // console.log(base);


        let profitOrLoss = (oldPriceFromPriceArr - newPricecFromPriceArr);

        console.log(` profitOrLoss${profitOrLoss}`);
        profitOrLossForRun += profitOrLoss;
        console.log(profitOrLossForRun);
        document.getElementById('betValue').textContent = '';
        document.getElementById('howMuch').value = '';


}


///render Price
function getNextPrice() {
        let nextPrice = Math.floor(Math.random() * 10);
        if (Math.random() > 0.5) {
                nextPrice *= -1;
        }
        base += nextPrice;
        return base;
}

function init() {


        for (let i = 0; i <= 5; i++) {
                const d = new Date();
                let secondBefore = 4 * (5 - i)
                d.setSeconds(d.getSeconds() - secondBefore);

                console.log(d);

                targetPriceArr.push(getNextPrice());
                targetDateArr.push(d.toLocaleTimeString());
        }
        newChart.update();

        changeCircle();
        run();
        timerFunction();
}

function renderPrice() {

        targetPriceArr.push(getNextPrice());
        newChart.update();
        console.log(targetPriceArr);

}


///////////renderDate 
function renderDate() {
        const d = new Date().toLocaleTimeString();
        if (targetPriceArr.length >= 8) {
                targetDateArr.shift();
                targetPriceArr.shift();
        }
        targetDateArr.push(d)
        newChart.update()


}

// setInterval(renderPrice, 4000);
// setInterval(renderDate, 4000)
///////timer

let timer = 0;

const timerFunction = () => {
        setInterval(function () {

                timer += 100;
                let second = parseInt(timer / 1000);
                // console.log(second);
                // console.log(second % 4) // 去到4000 微秒， %4   return 0 , secondInterger set to 0 
                let secondInteger = second % 4
                // if(secondInteger===0){
                //         secondInteger+=1;
                //         document.getElementById('timer').textContent = secondInteger;

                // }else{
                //         document.getElementById('timer').textContent = secondInteger;
                // }
                document.getElementById('timer').textContent = secondInteger;


        }, 100)

}

function payOutForBuy() {

        if (profitOrLossForRun > 0) {
                balance += bet * 2;
                document.getElementById('balance').textContent = balance;
                bet = 0;
                console.log(bet);
                document.getElementById('betValue').textContent = bet;
                buy = false;
        }
        else if (profitOrLossForRun < 0) {

                bet = 0;
                document.getElementById('betValue').textContent = bet;
        } else if (profitOrLossForRun === 0) {
                bet = 0;
                document.getElementById('betValue').textContent = bet;
        }
}
function payOutForSell() {

        if (profitOrLossForRun < 0) {
                balance += bet * 2;
                document.getElementById('balance').textContent = balance;
                bet = 0;
                console.log(bet);
                document.getElementById('betValue').textContent = bet;
                sell = false;
        } else if (profitOrLossForRun > 0) {

                bet = 0;
                document.getElementById('betValue').textContent = bet;
        } else if (profitOrLossForRun === 0) {
                bet = 0;
                document.getElementById('betValue').textContent = bet;
        }

}

const run = () => {
        setInterval(function () {

                renderDate();
                renderPrice();
                if (buy === true) {
                        caluProfitLoss(bet);
                        payOutForBuy(profitOrLossForRun);

                }
                if (sell === true) {
                        caluProfitLoss(bet);
                        payOutForSell(profitOrLossForRun);

                }
        }, 4000)
}
function changeCircle() {
        let count = 0;
        setInterval(function () {
                count++;
                // console.log(count);
                let firstCir = document.getElementById("firstCir")
                firstCir.style.strokeDashoffset = 440 - (110 * count);

                // if(count===3)
        }, 1000)


}
// changeCircle();
init();


